<?php

class WDYW {

    private static $initiated = false;
    private static $class_name = 'WDYW';

    /**
     * Plugin Initiate
     */
    public static function init() {
        if (!self::$initiated) {
            self::wdyw_init_hooks();
        }
    }

    /**
     * Initiate Hooks
     */
    private static function wdyw_init_hooks() {
        self::$initiated = true;
        self::wallpapers_init();
        self::create_custom_wallpaper_post();
        self::wdyw_options();
        self::create_upload_folder();
        self::copy_theme_files();

        add_action('admin_menu', array(self::$class_name, 'setting_menu'));
        add_action('wp_enqueue_scripts', array(self::$class_name, 'wdyw_assets'));
        add_action('wp_head', array(self::$class_name, 'wdyw_css_script'));
        add_action('wp_ajax_wdyw_cart_handler', array(self::$class_name, 'wdyw_cart_handler'));
        add_action('wp_ajax_nopriv_wdyw_cart_handler', array(self::$class_name, 'wdyw_cart_handler'));
        add_filter('woocommerce_cart_item_thumbnail', array(self::$class_name, 'cart_image'), 10, 3);
        add_filter('woocommerce_cart_item_name', array(self::$class_name, 'add_cart_items_name'), 10, 3);
        add_filter('woocommerce_add_cart_item_data', array(self::$class_name, 'add_cart_item_custom_data'), 10, 2);
        add_action('woocommerce_before_calculate_totals', array(self::$class_name, 'woo_custom_price'));
       add_action('woocommerce_add_order_item_meta', array(self::$class_name, 'order_meta_handler'), 1, 3);
        add_filter('woocommerce_add_to_cart_redirect', array(self::$class_name, 'cart_redirect'));
        add_action('woocommerce_add_to_cart', array(self::$class_name, 'woocommerce_empty_cart_before_add'), 0);
        add_action('woocommerce_before_checkout_form', array(self::$class_name, 'wallpaper_title_checkout'), 10);
		add_action( 'woocommerce_checkout_order_processed', array(self::$class_name, 'is_express_delivery'),  10, 10  );
		add_action( 'woocommerce_order_status_processing', array(self::$class_name, 'order_status_processing_mail'),  10, 10  );
		add_action( 'woocommerce_order_status_completed', array(self::$class_name, 'order_status_completed_mail'),  10, 10  );
        add_shortcode('wallpapers', array(self::$class_name, 'wallpaper_listing'));
        add_shortcode('custom_wallpaper', array(self::$class_name, 'custom_wallpaper'));
    }
		function order_status_completed_mail( $order_id ){
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		$result.= '<div style="border:2px solid grey;">';
		
			foreach ( $items as $item ) {
				$img_url = $item['item_meta']['Wallpaper Image URL'][0];
				$total += $item['line_subtotal'];
				$result.='<h1 style="background-color:#557da1;padding:36px 48px;color:white;">Your order is complete</h1><p style="padding:20px 20px 0px 20px;">Hi there. Your recent order on Printed walls has been completed. Your order details are shown below for your reference::</p>';
				$result.= '<div style="padding:20px;border-bottom: 1px solid grey;">';
				$result.= '<div><p><b>Wallpaper Title: </b>'.$item['item_meta']['Wallpaper Title'][0].'</p></div>';
				$result.= '<div><p><b>Width: </b>'.$item['Width'].'</p></div>';
				$result.= '<div><p><b>Height: </b>'.$item['Height'].'</p></div>';
				$result.= '<div><p><b>Unit: </b>'.$item['Unit'].'</p></div>';
				$result.= '<div><p><b>Finishing: </b>'.$item['Finishing'].'</p></div>';
				$result.= '<div><p><b>Delivery: </b>'.$item['Delivery'].'</p></div>';
				$result.= '<div><p><b>Meterial: </b>'.$item['Meterial'].'</p></div>';
				$result.= '<div><p><b>Quantity: </b>'.$item['qty'].'</p></div>';
				$result.= '<div><p><b>Subtotal: </b>'.$item['line_subtotal'].'</p></div>';
				$result.= '<div><p><b>Wallpaper Image URL:</b>'.$img_url.'</p></div>';
				$result.= '<div><p><b>Wallpaper Image</b><img src='.$img_url.'></p></div>';
				$result.= '</div>';
			}
			$result.= '</div>';
			$result.= '<div style="border:2px solid grey;padding:10px;margin-top:10px;"><h3>Total: '.$total.'</h3></div>';
			$admin_email   = get_option('admin_email');
			$billing_email = $order->billing_email;
			$billing_phone = $order->billing_phone;
			$billing_first_name = $order->billing_first_name;
			$billing_last_name = $order->billing_last_name;
			$billing_company = $order->billing_company;
			$billing_address_1 = $order->billing_address_1;
			$billing_address_2 = $order->billing_address_2;
			$billing_city = $order->billing_city;
			$billing_state = $order->billing_state;
			$billing_postcode = $order->billing_postcode;
			$billing_country = $order->billing_country;
			$shipping_first_name = $order->shipping_first_name;
			$shipping_last_name = $order->shipping_last_name;
			$shipping_company = $order->shipping_company;
			$shipping_address_1 = $order->shipping_address_1;
			$shipping_address_2 = $order->shipping_address_2;
			$shipping_city = $order->shipping_city;
			$shipping_state = $order->shipping_state;
			$shipping_postcode = $order->shipping_postcode;
			$shipping_country = $order->shipping_country;
			$result.= '<div style="border:2px solid grey;padding:0px 20px 20px;margin-top: 10px;">';
			$result.= '<div><h3>Customer details:</h3></div>';
			$result.= '<div><p><b>Email: </b>'.$billing_email.'</p></div>';
			$result.= '<div><p><b>Phone: </b>'.$billing_phone.'</p></div>';
			$result.= '</div>';
			$result.= '<div style="border: 2px solid grey;width: 43%; float: left;border-right: none;padding: 0px 20px 20px;margin-top:10px;"><h3>Billing Address Details:</h3>';
			$result.= '<p>'.$billing_first_name.'</p>';
			$result.= '<p>'.$billing_last_name.'</p>';
			$result.= '<p>'.$billing_address_1.'</p>';
			$result.= '<p>'.$billing_address_2.'</p>';
			$result.= '<p>'.$billing_city.'</p>';
			$result.= '<p>'.$billing_state.'</p>';
			$result.= '<p>'.$billing_postcode.'</p>';
			$result.= '<p>'.$billing_country.'</p>';
			$result.= '</div>';
			$result.= '<div style="border: 2px solid grey;border: 2px solid grey; width: 42%;float: left;padding: 0px 20px 20px;margin-top: 10px;"><h3>Shipping Address Details:</h3>';
			$result.= '<p>'.$shipping_first_name.'</p>';
			$result.= '<p>'.$shipping_last_name.'</p>';
			$result.= '<p>'.$shipping_address_1.'</p>';
			$result.= '<p>'.$shipping_address_2.'</p>';
			$result.= '<p>'.$shipping_city.'</p>';
			$result.= '<p>'.$shipping_state.'</p>';
			$result.= '<p>'.$shipping_postcode.'</p>';
			$result.= '<p>'.$shipping_country.'</p>';
			$result.= '</div>';
			$subject = 'Your Printed walls order from '.$order->order_date.'is complete';
			wc_mail( $billing_email, $subject, $result, $headers = "Content-Type: text/htmlrn", $attachments = "" );
}
  
	function order_status_processing_mail( $order_id ){
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		$result.= '<div style="border:2px solid grey;">';
		$result.='<h1 style="background-color:#557da1;padding:36px 48px;color:white;">Thank you for your order</h1><p style="padding:20px 20px 0px 20px;">Your order is on-hold until we confirm payment has been received. Your order details are shown below for your reference:</p>';
			foreach ( $items as $item ) {
				$img_url = $item['item_meta']['Wallpaper Image URL'][0];
				$total += $item['line_subtotal'];
				
				$result.= '<div style="padding:20px;border-bottom: 1px solid grey;">';
				$result.= '<div><p><b>Wallpaper Title: </b>'.$item['item_meta']['Wallpaper Title'][0].'</p></div>';
				$result.= '<div><p><b>Width: </b>'.$item['Width'].'</p></div>';
				$result.= '<div><p><b>Height: </b>'.$item['Height'].'</p></div>';
				$result.= '<div><p><b>Unit: </b>'.$item['Unit'].'</p></div>';
				$result.= '<div><p><b>Finishing: </b>'.$item['Finishing'].'</p></div>';
				$result.= '<div><p><b>Delivery: </b>'.$item['Delivery'].'</p></div>';
				$result.= '<div><p><b>Meterial: </b>'.$item['Meterial'].'</p></div>';
				$result.= '<div><p><b>Quantity: </b>'.$item['qty'].'</p></div>';
				$result.= '<div><p><b>Subtotal: </b>'.$item['line_subtotal'].'</p></div>';
				$result.= '<div><p><b>Wallpaper Image URL:</b>'.$img_url.'</p></div>';
				$result.= '<div><p><b>Wallpaper Image</b><img src='.$img_url.'></p></div>';
				$result.= '</div>';
			}
			$result.= '</div>';
			$result.= '<div style="border:2px solid grey;padding:10px;margin-top:10px;"><h3>Total: '.$total.'</h3></div>';
			$admin_email   = get_option('admin_email');
			$billing_email = $order->billing_email;
			$billing_phone = $order->billing_phone;
			$billing_first_name = $order->billing_first_name;
			$billing_last_name = $order->billing_last_name;
			$billing_company = $order->billing_company;
			$billing_address_1 = $order->billing_address_1;
			$billing_address_2 = $order->billing_address_2;
			$billing_city = $order->billing_city;
			$billing_state = $order->billing_state;
			$billing_postcode = $order->billing_postcode;
			$billing_country = $order->billing_country;
			$shipping_first_name = $order->shipping_first_name;
			$shipping_last_name = $order->shipping_last_name;
			$shipping_company = $order->shipping_company;
			$shipping_address_1 = $order->shipping_address_1;
			$shipping_address_2 = $order->shipping_address_2;
			$shipping_city = $order->shipping_city;
			$shipping_state = $order->shipping_state;
			$shipping_postcode = $order->shipping_postcode;
			$shipping_country = $order->shipping_country;
			$result.= '<div style="border:2px solid grey;padding:0px 20px 20px;margin-top: 10px;">';
			$result.= '<div><h3>Customer details:</h3></div>';
			$result.= '<div><p><b>Email: </b>'.$billing_email.'</p></div>';
			$result.= '<div><p><b>Phone: </b>'.$billing_phone.'</p></div>';
			$result.= '</div>';
			$result.= '<div style="border: 2px solid grey;width: 43%; float: left;border-right: none;padding: 0px 20px 20px;margin-top:10px;"><h3>Billing Address Details:</h3>';
			$result.= '<p>'.$billing_first_name.'</p>';
			$result.= '<p>'.$billing_last_name.'</p>';
			$result.= '<p>'.$billing_address_1.'</p>';
			$result.= '<p>'.$billing_address_2.'</p>';
			$result.= '<p>'.$billing_city.'</p>';
			$result.= '<p>'.$billing_state.'</p>';
			$result.= '<p>'.$billing_postcode.'</p>';
			$result.= '<p>'.$billing_country.'</p>';
			$result.= '</div>';
			$result.= '<div style="border: 2px solid grey;border: 2px solid grey; width: 42%;float: left;padding: 0px 20px 20px;margin-top: 10px;"><h3>Shipping Address Details:</h3>';
			$result.= '<p>'.$shipping_first_name.'</p>';
			$result.= '<p>'.$shipping_last_name.'</p>';
			$result.= '<p>'.$shipping_address_1.'</p>';
			$result.= '<p>'.$shipping_address_2.'</p>';
			$result.= '<p>'.$shipping_city.'</p>';
			$result.= '<p>'.$shipping_state.'</p>';
			$result.= '<p>'.$shipping_postcode.'</p>';
			$result.= '<p>'.$shipping_country.'</p>';
			$result.= '</div>';
			
			$subject = 'Your Printed walls order receipt from '.$order->order_date;
			wc_mail( $billing_email, $subject, $result, $headers = "Content-Type: text/htmlrn", $attachments = "" );

}
  
function is_express_delivery( $order_id ){
		$order = wc_get_order( $order_id );
		$items = $order->get_items();
		$result.= '<div style="border:2px solid grey;">';
		$result.='<h1 style="background-color:#557da1;padding:36px 48px;color:white;">New Printed Walls Order</h1><p style="padding:20px 20px 0px 20px;">Order details are shown below for your reference:</p>';
			foreach ( $items as $item ) {
				$img_url = $item['item_meta']['Wallpaper Image URL'][0];
				$total += $item['line_subtotal'];
				
				$result.= '<div style="padding:20px;border-bottom: 1px solid grey;">';
				$result.= '<div><p><b>Wallpaper Title: </b>'.$item['item_meta']['Wallpaper Title'][0].'</p></div>';
				$result.= '<div><p><b>Width: </b>'.$item['Width'].'</p></div>';
				$result.= '<div><p><b>Height: </b>'.$item['Height'].'</p></div>';
				$result.= '<div><p><b>Unit: </b>'.$item['Unit'].'</p></div>';
				$result.= '<div><p><b>Finishing: </b>'.$item['Finishing'].'</p></div>';
				$result.= '<div><p><b>Delivery: </b>'.$item['Delivery'].'</p></div>';
				$result.= '<div><p><b>Meterial: </b>'.$item['Meterial'].'</p></div>';
				$result.= '<div><p><b>Quantity: </b>'.$item['qty'].'</p></div>';
				$result.= '<div><p><b>Subtotal: </b>'.$item['line_subtotal'].'</p></div>';
				$result.= '<div><p><b>Wallpaper Image URL:</b>'.$img_url.'</p></div>';
				$result.= '<div><p><b>Wallpaper Image</b><img src='.$img_url.'></p></div>';
				$result.= '</div>';
			}
			$result.= '</div>';
			$result.= '<div style="border:2px solid grey;padding:10px;margin-top:10px;"><h3>Total: '.$total.'</h3></div>';
			$admin_email   = get_option('admin_email');
			$billing_email = $order->billing_email;
			$billing_phone = $order->billing_phone;
			$billing_first_name = $order->billing_first_name;
			$billing_last_name = $order->billing_last_name;
			$billing_company = $order->billing_company;
			$billing_address_1 = $order->billing_address_1;
			$billing_address_2 = $order->billing_address_2;
			$billing_city = $order->billing_city;
			$billing_state = $order->billing_state;
			$billing_postcode = $order->billing_postcode;
			$billing_country = $order->billing_country;
			$shipping_first_name = $order->shipping_first_name;
			$shipping_last_name = $order->shipping_last_name;
			$shipping_company = $order->shipping_company;
			$shipping_address_1 = $order->shipping_address_1;
			$shipping_address_2 = $order->shipping_address_2;
			$shipping_city = $order->shipping_city;
			$shipping_state = $order->shipping_state;
			$shipping_postcode = $order->shipping_postcode;
			$shipping_country = $order->shipping_country;
			$result.= '<div style="border:2px solid grey;padding:0px 20px 20px;margin-top: 10px;">';
			$result.= '<div><h3>Customer details:</h3></div>';
			$result.= '<div><p><b>Email: </b>'.$billing_email.'</p></div>';
			$result.= '<div><p><b>Phone: </b>'.$billing_phone.'</p></div>';
			$result.= '</div>';
			$result.= '<div style="border: 2px solid grey;width: 43%; float: left;border-right: none;padding: 0px 20px 20px;margin-top:10px;"><h3>Billing Address Details:</h3>';
			$result.= '<p>'.$billing_first_name.'</p>';
			$result.= '<p>'.$billing_last_name.'</p>';
			$result.= '<p>'.$billing_address_1.'</p>';
			$result.= '<p>'.$billing_address_2.'</p>';
			$result.= '<p>'.$billing_city.'</p>';
			$result.= '<p>'.$billing_state.'</p>';
			$result.= '<p>'.$billing_postcode.'</p>';
			$result.= '<p>'.$billing_country.'</p>';
			$result.= '</div>';
			$result.= '<div style="border: 2px solid grey;border: 2px solid grey; width: 42%;float: left;padding: 0px 20px 20px;margin-top: 10px;"><h3>Shipping Address Details:</h3>';
			$result.= '<p>'.$shipping_first_name.'</p>';
			$result.= '<p>'.$shipping_last_name.'</p>';
			$result.= '<p>'.$shipping_address_1.'</p>';
			$result.= '<p>'.$shipping_address_2.'</p>';
			$result.= '<p>'.$shipping_city.'</p>';
			$result.= '<p>'.$shipping_state.'</p>';
			$result.= '<p>'.$shipping_postcode.'</p>';
			$result.= '<p>'.$shipping_country.'</p>';
			$result.= '</div>';
			$subject_admin = 'Printed Walls New Customer Order ('.$order_id.')- '.$order->order_date;
			$subject_customer = 'Printed Walls New Order ('.$order_id.')- '.$order->order_date;
			wc_mail( $admin_email, $subject_admin, $result, $headers = "Content-Type: text/htmlrn", $attachments = "" );
			wc_mail( $billing_email, $subject_customer, $result, $headers = "Content-Type: text/htmlrn", $attachments = "" );

}
    public static function wallpaper_title_checkout() {
        global $woocommerce;                
        $inner_html = NULL;
        foreach ($woocommerce->cart->cart_contents as $key => $cart_content) {
			$url = $cart_content['wallpaper_url'];
			$string= str_replace('http://printedwalls.co.uk/Wallpapers/', "", $url);
			$slug = str_replace('-'," ", $string);
			$title = ucwords($slug);
			$title= stripslashes($title);
			$title = str_replace('/',"", $title);
			$inner_html.= "<p>".$title."</p>"."<p>".$cart_content['width'].' '.ucfirst($cart_content['unit']).' &#10005; '.$cart_content['height'].' '.ucfirst($cart_content['unit']).' ('.ucfirst($cart_content['finishing']).")<strong class='product-quantity'>  x " . $cart_content['quantity'] . "</strong></p>";
        }        
        ?>
        <div class='checkout_wallpaper' style='display:none;'>
            <?php echo $inner_html; ?>
        </div>
        <?php 
		
    }

    public static function create_custom_wallpaper_post() {
        if (!get_option('wdyw_custom_wallpaper')) {
            $new_post = array(
                'ID' => '',
                'post_type' => 'wallpaper', // Custom Post Type Slug
                'post_status' => 'publish',
                'post_title' => 'Custom Wallpaper',
            );

            $post_id = wp_insert_post($new_post);

            add_option('wdyw_custom_wallpaper', $post_id);
        }
    }

    public static function custom_wallpaper($atts) {
        $upload_directory = wp_upload_dir();

        $error = NULL;

        if ($_FILES['file_upload']['tmp_name']) {
            if ($_FILES['file_upload']['error'] > 0) {
                $error = 'An error ocurred when uploading.';
            }

            if ($_FILES['file_upload']['type'] != 'image/jpeg') {
                $error = 'Only JPG file supported';
            }

            if ($error) {
                echo $error;
            } else {
                $rand = rand();
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $filename = $rand . $datetime . ".jpg";
                $filename_resized = $rand . $datetime . "-resized.jpg";
                if (!move_uploaded_file($_FILES['file_upload']['tmp_name'], $upload_directory['basedir'] . '/custom-wallpapers/' . $filename)) {
                    $error = "Error in uploading file";
                }

                include('class.simple-image.php');
                $image = new SimpleImage();
                $image->load($upload_directory['basedir'] . '/custom-wallpapers/' . $filename);
                $image->resize(722, 508);
                $image->save($upload_directory['basedir'] . '/custom-wallpapers/' . $filename_resized);

                $url = get_permalink(get_option('wdyw_custom_wallpaper'));
                $query = parse_url($url, PHP_URL_QUERY);

                // Returns a string if the URL has parameters or NULL if not
                if ($query) {
                    $url .= '&filename=' . urlencode($upload_directory['baseurl'] . '/custom-wallpapers/' . $filename_resized);
                } else {
                    $url .= '?filename=' . urlencode($upload_directory['baseurl'] . '/custom-wallpapers/' . $filename_resized);
                }
                ?>
                <script>
                    window.location = '<?php echo $url; ?>';
                </script> 
                <?php
            }
        }
        include('html/upload.php');
    }

    public static function wallpaper_listing($atts) {
        $query = self::get_wallpapers();
        include('html/listing.php');
    }

    public static function get_wallpapers() {
        $args = array(
            'post_type' => 'wallpaper',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'caller_get_posts' => 1
        );
        $query = null;
        $query = new WP_Query($args);
        return $query;
    }

    public static function copy_theme_files() {
        $theme_directory = get_template_directory();
        if (!file_exists($theme_directory . "/single-wallpaper.php")) {
            copy(plugin_dir_path(__FILE__) . "/theme_files/single-wallpaper.php", $theme_directory . "/single-wallpaper.php");
        }
        if (!file_exists($theme_directory . "/archive-wallpaper.php")) {
            copy(plugin_dir_path(__FILE__) . "/theme_files/archive-wallpaper.php", $theme_directory . "/archive-wallpaper.php");
        }
    }

    public static function create_upload_folder() {
        $upload_dir = wp_upload_dir();

        if (!is_dir($upload_dir['basedir'] . "/design-your-wall/")) {
            //Directory does not exist, so lets create it.
            mkdir($upload_dir['basedir'] . "/design-your-wall/", 0755);
        }

        if (!is_dir($upload_dir['basedir'] . "/custom-wallpapers/")) {
            //Directory does not exist, so lets create it.
            mkdir($upload_dir['basedir'] . "/custom-wallpapers/", 0755);
        }
    }

    public static function woocommerce_empty_cart_before_add() {
        global $woocommerce;

        // Get 'product_id' and 'quantity' for the current woocommerce_add_to_cart operation
        $prodId = (int) $_POST["add-to-cart"];
        $prodQty = (int) $_POST["quantity"];

        // Get the total of items for each product in cart
        $cartQty = $woocommerce->cart->get_cart_item_quantities();

        // Empty cart before adding the new product to the cart
        if ($cartQty[$prodId] != $prodQty) {
            $woocommerce->cart->empty_cart();
            $woocommerce->cart->add_to_cart($prodId, $prodQty);
        }
    }

    public static function cart_redirect() {
        global $woocommerce;
        return $woocommerce->cart->get_cart_url();
    }

    public static function order_meta_handler($item_id, $values, $cart_item_key) {
        $cart_session = WC()->session->get('cart');
		wc_add_order_item_meta($item_id, "Wallpaper Title", $cart_session[$cart_item_key]['wallpaper_title']);
        wc_add_order_item_meta($item_id, "Orignal Image", "<img src='" . $cart_session[$cart_item_key]['full_image'] . "'>");
        wc_add_order_item_meta($item_id, "Resized Image", "<img src='" . $cart_session[$cart_item_key]['cropped_image'] . "'>");
        wc_add_order_item_meta($item_id, "Width", $cart_session[$cart_item_key]['width']);
        wc_add_order_item_meta($item_id, "Height", $cart_session[$cart_item_key]['height']);
        wc_add_order_item_meta($item_id, "Unit", $cart_session[$cart_item_key]['unit']);
        wc_add_order_item_meta($item_id, "Finishing", $cart_session[$cart_item_key]['finishing']);
        wc_add_order_item_meta($item_id, "Delivery", $cart_session[$cart_item_key]['delivery_opt']);
        wc_add_order_item_meta($item_id, "Meterial", $cart_session[$cart_item_key]['material']);
        wc_add_order_item_meta($item_id, "Wallpaper URL", $cart_session[$cart_item_key]['wallpaper_url']);
        wc_add_order_item_meta($item_id, "Wallpaper Image URL", $cart_session[$cart_item_key]['cropped_image']);
    }

    public static function cart_image($thumb, $current_cart_item, $current_cart_item_key) {
        global $woocommerce;
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            if ($cart_item['data']->id == get_option('wdyw_product')) {
                $cart_session = WC()->session->get('cart');
                $thumb_size = get_option("shop_thumbnail_image_size");
                return "<img src='" . $cart_session[$current_cart_item_key]['cropped_image'] . "' width='" . $thumb_size['width'] . "' height='" . $thumb_size['height'] . "'>";
            }
        }
    }

    public static function woo_custom_price() {
        global $woocommerce;
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            if ($cart_item['data']->id == get_option('wdyw_product')) {
                $cart_session = WC()->session->get('cart');
                $cart_item['data']->set_price($cart_session[$cart_item_key]['total_price']);
            }
        }
    }

    public static function add_cart_item_custom_data($cart_item_meta, $product_id) {
        if ($product_id == get_option('wdyw_product')) {
            global $woocommerce;
            $upload_dir = wp_upload_dir();
            $cart_item_meta['full_image'] = $_GET['full_image'];
            $cart_item_meta['cropped_image'] = $upload_dir['baseurl'] . "/design-your-wall/" . $_GET['filename'];
            $cart_item_meta['width'] = $_GET['width'];
            $cart_item_meta['height'] = $_GET['height'];
            $cart_item_meta['unit'] = $_GET['unit'];
            $cart_item_meta['total_price'] = $_GET['total_price'];
            $cart_item_meta['finishing'] = $_GET['finishing'];
            $cart_item_meta['delivery_opt'] = $_GET['delivery_opt'];
            $cart_item_meta['material'] = $_GET['material'];
            $cart_item_meta['wallpaper_title'] = $_GET['wallpaper_title'];
            $cart_item_meta['wallpaper_url'] = $_GET['wallpaper_url'];
            return $cart_item_meta;
        }
    }

    public static function add_cart_items_name($product_link, $cart_item, $cart_item_key) {
			$cart_session = WC()->session->get('cart');
			$product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
			$url = $cart_session[$cart_item_key]['wallpaper_url'];
			$wallpaper_title = $cart_session[$cart_item_key]['wallpaper_title'];
			$finishing_str = $cart_session[$cart_item_key]['finishing'];
			$delivery_opt = $cart_session[$cart_item_key]['delivery_opt'];
			$material_str = $cart_session[$cart_item_key]['material'];
			$finishing = str_replace('_', ' ', $finishing_str);
			$material = str_replace('_', ' ', $material_str);
			$string= str_replace('http://printedwalls.co.uk/Wallpapers/', "", $url);
			$slug = str_replace('-'," ", $string);
			$title = ucwords($slug);
			$title= stripslashes($title);
			$title = str_replace('/',"", $title);
        if ($cart_item_key) {
			if (strpos($url, 'custom-wallpaper') !== false) {
				 echo "<p>Custom Wallaper</p>";
			}else{
				echo "<p>".$title."</p>";
			}
            echo "<p>".$cart_session[$cart_item_key]['width'].' '.ucfirst($cart_session[$cart_item_key]['unit']).' &#10005; '.$cart_session[$cart_item_key]['height'].' '.ucfirst($cart_session[$cart_item_key]['unit']).'</br>Material: '.ucfirst($material).'</br>Finishing:'.ucfirst($finishing).'</br>Delivery: '.ucfirst($delivery_opt)."</p>";
        } 
    }

    public static function wdyw_cart_handler() {
        if ($_POST['data']) {
            $source_x = $_POST['data']['x'];
            $source_y = $_POST['data']['y'];
            $width = $_POST['data']['w'];
            $height = $_POST['data']['h'];

            $dest = imagecreatetruecolor($width, $height);

            $src = imagecreatefromjpeg($_POST['data']['image']);

            imagecopy($dest, $src, 0, 0, $source_x, $source_y, $width, $height);

            $upload_dir = wp_upload_dir();

            $filename = strtotime(date('Y-m-d H:i:s')) . rand() . ".jpg";

            $cropped_image = $upload_dir['basedir'] . "/design-your-wall/" . $filename;

            imagejpeg($dest, $cropped_image, 100);

            global $woocommerce;
            $cart_url = $woocommerce->cart->get_cart_url();

            echo json_encode(array('cart_utl' => $cart_url, 'filename' => $filename, 'product_id' => get_option('wdyw_product')));
        }
        wp_die();
    }

    /**
     * Default plugin options
     */
    public static function wdyw_options() {
        if (!get_option('wdyw_default_width')) {
            add_option('wdyw_default_width', 6.00);
        }
        if (!get_option('wdyw_default_height')) {
            add_option('wdyw_default_height', 8.00);
        }
        if (!get_option('wdyw_min_price')) {
            add_option('wdyw_min_price', 2);
        }
        if (!get_option('wdyw_finishing_one_piece')) {
            add_option('wdyw_finishing_one_piece', 20);
        }
        if (!get_option('wdyw_100_200_discount')) {
            add_option('wdyw_100_200_discount', 10);
        }
        if (!get_option('wdyw_201_300_discount')) {
            add_option('wdyw_201_300_discount', 20);
        }
        if (!get_option('wdyw_301_400_discount')) {
            add_option('wdyw_301_400_discount', 30);
        }
        if (!get_option('wdyw_401_500_discount')) {
            add_option('wdyw_401_500_discount', 40);
        }
        if (!get_option('wdyw_500_plus_discount')) {
            add_option('wdyw_500_plus_discount', 50);
        }
        if (!get_option('wdyw_max_width')) {
            add_option('wdyw_max_width', 100);
        }
        if (!get_option('wdyw_max_height')) {
            add_option('wdyw_max_height', 50);
        }
        if (!get_option('wdyw_minimum_width')) {
            add_option('wdyw_minimum_width', 6.00);
        }
        if (!get_option('wdyw_minimum_height')) {
            add_option('wdyw_minimum_height', 8.00);
        }
    }

    /**
     * Register and add Stylesheet and Javascript
     */
    public static function wdyw_assets() {
        wp_enqueue_style('wdyw_css', plugins_url() . '/wc-design-your-wall/assets/css/style.css');
        wp_enqueue_style('Jcrop', plugins_url() . '/wc-design-your-wall/assets/css/Jcrop.css');
        wp_enqueue_script('Jcrop', plugins_url() . '/wc-design-your-wall/assets/js/Jcrop.js', array('jquery'), rand(), true);
        wp_enqueue_script('wydw_js', plugins_url() . '/wc-design-your-wall/assets/js/wdyw.js', array('jquery'), rand(), true);
    }

    /**
     * Wallpapers Setting Menu
     */
    public static function setting_menu() {
        add_options_page('Wallpapers Settings', 'Wallpapers Settings', 'manage_options', 'wdyw-settings', array(self::$class_name, 'setting_page'));
    }

    /**
     * Wallpapers Setting Page
     */
    public static function setting_page() {
        if (isset($_POST['wdyw_save_settings'])) {
           /*  if (get_option('wdyw_product')) {
                
            } else {
                add_option('wdyw_product', $_POST['wdyw_product']);
            } */
			update_option('wdyw_product', $_POST['wdyw_product']);
            update_option('wdyw_min_price', $_POST['wdyw_min_price']);
            update_option('wdyw_default_width', $_POST['wdyw_default_width']);
            update_option('wdyw_default_height', $_POST['wdyw_default_height']);
            update_option('wdyw_finishing_one_piece', $_POST['wdyw_finishing_one_piece']);
            update_option('wdyw_100_200_discount', $_POST['wdyw_100_200_discount']);
            update_option('wdyw_201_300_discount', $_POST['wdyw_201_300_discount']);
            update_option('wdyw_301_400_discount', $_POST['wdyw_301_400_discount']);
            update_option('wdyw_401_500_discount', $_POST['wdyw_401_500_discount']);
            update_option('wdyw_500_plus_discount', $_POST['wdyw_500_plus_discount']);
            update_option('wdyw_max_width', $_POST['wdyw_max_width']);
            update_option('wdyw_max_height', $_POST['wdyw_max_height']);
            update_option('wdyw_minimum_width', $_POST['wdyw_minimum_width']);
            update_option('wdyw_minimum_height', $_POST['wdyw_minimum_height']);
            $updated = 1;
        }
        $current_product = get_option('wdyw_product');
        $products = self::get_woocommerce_product_list();
        $current_wallpaper = get_option('wdyw_custom_wallpaper');
        $wallpapers = self::get_wallpapers();
        include('html/settings.php');
    }

    /**
     * Get all woocommerce products
     * @return type
     */
    public static function get_woocommerce_product_list() {
        $full_product_list = array();
        $loop = new WP_Query(array('post_type' => array('product', 'product_variation'), 'posts_per_page' => -1));

        while ($loop->have_posts()) : $loop->the_post();
            $theid = get_the_ID();
            $product = new WC_Product($theid);
            // its a variable product
            if (get_post_type() == 'product_variation') {
                $parent_id = wp_get_post_parent_id($theid);
                $sku = get_post_meta($theid, '_sku', true);
                $thetitle = get_the_title($parent_id);

                // ****** Some error checking for product database *******
                // check if variation sku is set
                if ($sku == '') {
                    if ($parent_id == 0) {
                        // Remove unexpected orphaned variations.. set to auto-draft
                        $false_post = array();
                        $false_post['ID'] = $theid;
                        $false_post['post_status'] = 'auto-draft';
                        wp_update_post($false_post);
                        if (function_exists(add_to_debug))
                            add_to_debug('false post_type set to auto-draft. id=' . $theid);
                    } else {
                        // there's no sku for this variation > copy parent sku to variation sku
                        // & remove the parent sku so the parent check below triggers
                        $sku = get_post_meta($parent_id, '_sku', true);
                        if (function_exists(add_to_debug))
                            add_to_debug('empty sku id=' . $theid . 'parent=' . $parent_id . 'setting sku to ' . $sku);
                        update_post_meta($theid, '_sku', $sku);
                        update_post_meta($parent_id, '_sku', '');
                    }
                }
                // ****************** end error checking *****************
                // its a simple product
            } else {
                $sku = get_post_meta($theid, '_sku', true);
                $thetitle = get_the_title();
            }
            // add product to array but don't add the parent of product variations
            if (!empty($sku))
                $full_product_list[] = array($thetitle, $sku, $theid);
        endwhile;
        wp_reset_query();
        // sort into alphabetical order, by title
        sort($full_product_list);
        return $full_product_list;
    }

    /**
     * Wallpapers Post Setup
     */
    public static function wallpapers_init() {
        //Create Wallaper CPT
        $support = array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments');
        self::custom_post('wallpaper', 'Wallpapers', 'Wallpaper', 'post', 'wallpaper_metaboxes', $support, $args, $labels);
        //Create Wallpapers Cateogry for wallpaper CPT
        self::custom_taxonomy('wallpapers-category', 'Categories', 'Category', 'wallpaper');
    }

    /**
     * Custom Post Type
     * @param type $post_name
     * @param type $plural_name
     * @param type $singular_name
     * @param type $capability_type
     * @param type $callback_cm
     * @param type $support
     * @param type $args
     * @param type $labels
     */
    public static function custom_post($post_name, $plural_name, $singular_name, $capability_type, $callback_cm, $support, $args = NULL, $labels = NULL) {
        $default_labels = array(
            'name' => _x($plural_name, 'post type general name'),
            'singular_name' => _x($singular_name, 'post type singular name'),
            'menu_name' => _x($plural_name, 'admin menu'),
            'name_admin_bar' => _x($singular_name, 'add new on admin bar'),
            'add_new' => _x('Add New', $singular_name),
            'add_new_item' => __('Add New ') . $singular_name,
            'new_item' => __('New ') . $singular_name,
            'edit_item' => __('Edit ') . $singular_name,
            'view_item' => __('View ') . $singular_name,
            'all_items' => __('All ') . $plural_name,
            'search_items' => __('Search ') . $plural_name,
            'not_found' => __('No ') . $singular_name . (' found.'),
            'not_found_in_trash' => __('No ') . $singular_name . (' found in Trash.')
        );

        if (!empty($labels)) {
            $default_labels = array_replace($default_labels, $labels);
        }

        $default_args = array(
            'labels' => $default_labels,
            'description' => __('Description.'),
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => $plural_name),
            'with_front' => true,
            'capability_type' => $capability_type,
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'register_meta_box_cb' => array(self::$class_name, $callback_cm),
            'supports' => $support
        );

        if (!empty($args)) {
            $default_args = array_replace($default_args, $args);
        }

        register_post_type($post_name, $default_args);
    }

    /**
     * Custom Taxonomy
     * @param type $taxonomy_name
     * @param type $plural_name
     * @param type $singular_name
     * @param type $post_type
     * @param type $args
     * @param type $labels
     */
    public static function custom_taxonomy($taxonomy_name, $plural_name, $singular_name, $post_type, $args = NULL, $labels = NULL) {
        $default_labels = array(
            'name' => $plural_name,
            'singular_name' => $singular_name,
            'search_items' => 'Search ' . $plural_name,
            'all_items' => 'All ' . $plural_name,
            'edit_item' => 'Edit ' . $singular_name,
            'update_item' => 'Update ' . $singular_name,
            'add_new_item' => 'Add New ' . $singular_name,
            'new_item_name' => 'New ' . $singular_name . ' Name',
            'menu_name' => $singular_name,
        );

        if (!empty($labels)) {
            $default_labels = array_replace($default_labels, $labels);
        }

        $default_args = array(
            'labels' => $default_labels,
            'new_item_name' => 'Add New ' . $singular_name,
            'public' => true,
            'rewrite' => true,
            'hierarchical' => true,
        );

        if (!empty($args)) {
            $default_args = array_replace($default_args, $args);
        }
        register_taxonomy($taxonomy_name, $post_type, $default_args);
    }

    public static function wallpaper_metaboxes() {
        
    }

    /**
     * Javascript & CSS in WP Head
     */
    public static function wdyw_css_script() {
        ?>        
        <style>
            #text-inputs { margin: 10px 8px 0; }
            .input-group { margin-right: 1.5em; }
            .nav-box {display:none; width: 748px; padding: 0 !important; margin: 4px 0; background-color: #f8f8f7; }

        </style>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            var min_width = '<?php echo get_option('wdyw_minimum_width'); ?>';
            var min_height = '<?php echo get_option('wdyw_minimum_height'); ?>';
            var max_width = '<?php echo get_option('wdyw_max_width'); ?>';
            var max_height = '<?php echo get_option('wdyw_max_height'); ?>';
            var sq_feet_price = '<?php echo get_option('wdyw_min_price'); ?>';
            var one_piece_per = '<?php echo get_option('wdyw_finishing_one_piece'); ?>';
            var dis_100_200 = '<?php echo get_option('wdyw_100_200_discount'); ?>';
            var dis_201_300 = '<?php echo get_option('wdyw_201_300_discount'); ?>';
            var dis_301_400 = '<?php echo get_option('wdyw_301_400_discount'); ?>';
            var dis_401_500 = '<?php echo get_option('wdyw_401_500_discount'); ?>';
            var dis_500_plus = '<?php echo get_option('wdyw_500_plus_discount'); ?>';
        </script>
        <?php
    }

}
