<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<?php if ( have_posts() ) : ?>
    
    	<h1 class="page-title">Select Your Design</h1>
        
        <table border="0" cellpadding="5" cellspacing="5" width="100%">
        	<tr>
				<?php
				$x=1;		
                while ( have_posts() ) : the_post();				
                ?>
                	<td width="33%">
                    	<?php the_post_thumbnail('thumbnail'); ?><br />
						<?php the_title( sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
                    </td>  
                <?php
				if($x == 4){
					echo "</tr><tr>";
				}
				$x++;
                endwhile;
                ?>
        	</tr>
        </table>
    <?php    
	endif;
	
get_footer(); ?>
