var curunit = "foot";

function unitConversion(e) {
    var t = document.getElementById("design_yout_wall_width").value,
            i = document.getElementById("design_yout_wall_height").value;
    if ("cm" == e) {
        if ("mm" == curunit)
            var n = t / 10,
                u = i / 10;
        else if ("m" == curunit)
            var n = 100 * t,
                u = 100 * i;
        else if ("foot" == curunit)
            var n = 30.48 * t,
                u = 30.48 * i;
        else if ("inches" == curunit)
            var n = t / .3937,
                u = i / .3937;
        curunit = "cm", unit_name = "CM"
    } else if ("m" == e) {
        if ("mm" == curunit)
            var n = t / 1e3,
                u = i / 1e3;
        else if ("cm" == curunit)
            var n = t / 100,
                u = i / 100;
        else if ("foot" == curunit)
            var n = .3048 * t,
                u = .3048 * i;
        else if ("inches" == curunit)
            var n = t / 39.370078740157,
                u = i / 39.370078740157;
        curunit = "m", unit_name = "M"
    } else if ("mm" == e) {
        if ("cm" == curunit)
            var n = 10 * t,
                u = 10 * i;
        else if ("m" == curunit)
            var n = 1e3 * t,
                u = 1e3 * i;
        else if ("foot" == curunit)
            var n = 304.8 * t,
                u = 304.8 * i;
        else if ("inches" == curunit)
            var n = t / .03937,
                u = i / .03937;
        curunit = "mm", unit_name = "MM"
    } else if ("foot" == e) {
        if ("cm" == curunit)
            var n = t / 30.48,
                u = i / 30.48;
        else if ("m" == curunit)
            var n = t / .3048,
                u = i / .3048;
        else if ("mm" == curunit)
            var n = t / 304.8,
                u = i / 304.8;
        else if ("inches" == curunit)
            var n = t / 12,
                u = i / 12;
        curunit = "foot", unit_name = "Ft"
    } else if ("inches" == e) {
        if ("foot" == curunit)
            var n = 12 * t,
                u = 12 * i;
        else if ("cm" == curunit)
            var n = .3937 * t,
                u = .3937 * i;
        else if ("m" == curunit)
            var n = 39.370078740157 * t,
                u = 39.370078740157 * i;
        else if ("mm" == curunit)
            var n = .03937 * t,
                u = .03937 * i;
        curunit = "inches", unit_name = "In"
    }
    n = n.toFixed(2), u = u.toFixed(2), document.getElementById("design_yout_wall_width").value = n, document.getElementById("design_yout_wall_height").value = u, jQuery(".unit_value").text(unit_name), jQuery("#lable_height").html("<p>"+ n + "&nbsp;" + unit_name+"</p>"), jQuery("#lable_width").html("<p>"+ u + "&nbsp;" + unit_name+"</p>")
}

jQuery(document).ready(function(){
var total_price = document.getElementById('total_price').innerHTML;
	var product_price_total = document.getElementById('total_product_price').value = total_price;
});
function set_finishing(){
	var product_price = document.getElementById('total_product_price').value;
	var finishing = document.getElementById('finishing').value;
	console.log(finishing);
	if(finishing=='one_piece'){
		finishing_price = product_price * 20 / 100;
	}else if(finishing=='tiles'){
		finishing_price = '0';
	}
	var finishing_total_price = document.getElementById('finishing_total_price').value = finishing_price;
	calculate_total();
}
function set_material(){
	var product_price = document.getElementById('total_product_price').value;
	var material = document.getElementById('material').value;
	console.log(material);
	if(material=='polycril_paper'){
		material_price = product_price * 10 / 100;
		total_price = parseFloat(material_price) + parseFloat(product_price);
	}else if(material=='pvc'){
		material_price = '0';
		total_price =parseFloat(product_price);
	}
	var material_total_price = document.getElementById('material_total_price').value = material_price;
	calculate_total();
}
function set_delivery(){
	var product_price = document.getElementById('total_product_price').value;
	var delivery = document.getElementById('delivery').value;
	if(delivery == '72hrs'){
		delivery_price = product_price * 15 / 100;
		total_price = parseFloat(delivery_price) + parseFloat(product_price);
	}else if(delivery == '48hrs'){
		delivery_price = product_price * 25 / 100;
		total_price = parseFloat(delivery_price) + parseFloat(product_price);
	}else if(delivery=='standard'){
		delivery_price = '0';
		total_price = parseFloat(product_price);
	}
	var delivery_total_price = document.getElementById('delivery_total_price').value = delivery_price;
	calculate_total();
}
function calculate_total(){
	var product_price = document.getElementById('total_product_price').value;
	finishing_total_price = document.getElementById('finishing_total_price').value;
	delivery_total_price = document.getElementById('delivery_total_price').value;
	material_total_price = document.getElementById('material_total_price').value;
	console.log(product_price);
	console.log(delivery_total_price);
	console.log(material_total_price);
	console.log(finishing_total_price);
	var total_price = parseFloat(product_price) + parseFloat(delivery_total_price)+ parseFloat(material_total_price) + parseFloat(finishing_total_price);
	
	document.getElementById('total_price').innerHTML = total_price.toFixed(2);	
}
function updateCoords(e) {
}

function wdyw_add_to_cart() {
    var e = .3 * min_width,
            t = .3 * min_height,
            i = 304.8 * min_width,
            n = 304.8 * min_height,
            u = 12 * min_width,
            r = 12 * min_height,
            a = 30.48 * min_width,
            l = 30.48 * min_height,
            m = document.getElementById("design_yout_wall_width").value,
            c = document.getElementById("design_yout_wall_height").value,
            o = jQuery("#unit").val();

    var me = .3 * max_width,
            mt = .3 * max_height,
            mi = 304.8 * max_width,
            mn = 304.8 * max_height,
            mu = 12 * max_width,
            mr = 12 * max_height,
            ma = 30.48 * max_width,
            ml = 30.48 * max_height;

    switch (o) {
        case "foot":
            if (parseInt(m) < parseInt(min_width) || parseInt(c) < parseInt(min_height))
                return alert("Width and Height must be greater then or equal to " + min_width + " x " + min_height + " Foot"), !1;
            if (parseInt(m) > parseInt(max_width) || parseInt(c) > parseInt(max_height))
                return alert("Width and Height must be less then or equal to " + max_width + " x " + max_height + " Foot"), !1;
            break;
        case "cm":
            if (a > m || l > c)
                return alert("Width and Height must be greater then or equal to " + a + " x " + l + " CM"), !1;
            if (ma < m || ml < c)
                return alert("Width and Height must be less then or equal to " + ma + " x " + ml + " CM"), !1;
            break;
        case "m":
            if (e > m || t > c)
                return alert("Width and Height must be greater then or equal to " + e + " x " + t + " Meters"), !1;
            if (me < m || mt < c)
                return alert("Width and Height must be less then or equal to " + me + " x " + mt + " Meters"), !1;
            break;
        case "mm":
            if (i > m || n > c)
                return alert("Width and Height must be greater then or equal to " + i + " x " + n + " Mili Meters"), !1;
            if (mi < m || mn < c)
                return alert("Width and Height must be less then or equal to " + mi + " x " + mn + " Mili Meters"), !1;
            break;
        case "inches":
            if (u > m || r > c)
                return alert("Width and Height must be greater then or equal to " + u + " x " + r + " Inches"), !1
            if (mu < m || mr < c)
                return alert("Width and Height must be less then or equal to " + mu + " x " + mr + " Inches"), !1
    }
    var d = document.getElementById("crop-x").value,
            h = document.getElementById("crop-y").value,
            s = document.getElementById("crop-w").value,
            _ = document.getElementById("crop-h").value,
            f = document.getElementById("image-path").value,
            g = document.getElementById("unit").value,
            v = document.getElementById("finishing").value,
            material = document.getElementById("material").value,
            delvr = document.getElementById("delivery").value,
            wt = jQuery("#wallpaper_title").text(),
            y = jQuery("#total_price").text(),
            w = jQuery("#target").attr("src"),
            url = window.location.href,
            I = {
                action: "wdyw_cart_handler",
                data: {
                    x: d,
                    y: h,
                    w: s,
                    h: _,
                    image: f
                }
            };
    jQuery.post(ajaxurl, I, function(e) {
        var t = JSON.parse(e);
        var wallpaper_url = window.location.href;
        window.location = "?add-to-cart=" + t.product_id + "&filename=" + t.filename + "&width=" + m + "&height=" + c + "&unit=" + g + "&finishing=" + v + "&delivery_opt=" + delvr + "&material=" + material + "&total_price=" + y + "&full_image=" + w + "&wallpaper_title=" + wt + "&wallpaper_url=" + url + "&wallpaper_url=" + wallpaper_url
    })
}

function updateFinishingPrice() {
    jQuery("#design_yout_wall_width").keyup()
}

jQuery(document).ready(function() {
    jQuery('.cart .cart_item .product-thumbnail').each(function() {
        var product_url = jQuery(this).next('.product-name').children('a').attr('href');
        jQuery('.cart .product-thumbnail a').attr('href', product_url);
    });

    var count = 0;
    setInterval(function() {

        if (jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-name').hasClass('added')) {
            count = 1;
        }

        if (count == 0) {
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-name').html(jQuery('.checkout_wallpaper').html());
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-name').addClass('added');
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item').hide();
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item:first-child').show();           
            jQuery('.woocommerce-checkout-review-order-table tbody .cart_item .product-total').text(jQuery('.cart-subtotal .amount').text());
        }
    }, 3000);

});