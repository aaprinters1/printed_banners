<?php

/*

Plugin Name: Printed Walls

*/
if (!function_exists('work_post_type')) {

    function work_post_type()
    {

        $labels = array(
            'name' => _x('Works', 'Post Type General Name', 'rast'),
            'singular_name' => _x('Work', 'Post Type Singular Name', 'rast'),
            'menu_name' => __('Work', 'rast'),
            'name_admin_bar' => __('Work', 'rast'),
            'parent_item_colon' => __('Parent Work:', 'rast'),
            'all_items' => __('All Works', 'rast'),
            'add_new_item' => __('Add New Work', 'rast'),
            'add_new' => __('Add Work', 'rast'),
            'new_item' => __('New Work', 'rast'),
            'edit_item' => __('Edit Work', 'rast'),
            'update_item' => __('Update Work', 'rast'),
            'view_item' => __('View Work', 'rast'),
            'search_items' => __('Search Work', 'rast'),
            'not_found' => __('Work Not found', 'rast'),
            'not_found_in_trash' => __('Not work found in Trash', 'rast'),
        );
        $rewrite = array(
            'slug' => 'work',
            'with_front' => true,
            'pages' => true,
            'feeds' => true,
        );
        $args = array(
            'label' => __('Work', 'rast'),
            'description' => __('List of Works', 'rast'),
            'labels' => $labels,
            'supports' => array('title', 'thumbnail', 'revisions',),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 7,
            'menu_icon' => 'dashicons-admin-post',
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => 'work',
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'rewrite' => $rewrite,
            'capability_type' => 'post',
        );
        register_post_type('work', $args);
    }

    add_action('init', 'work_post_type', 0);
}

/*
 * Register CATAGORY for Post Type WORK
 */
if (!function_exists('work_category')) {

    function work_category()
    {

        $labels = array(
            'name' => _x('Categories', 'Taxonomy General Name', 'rast'),
            'singular_name' => _x('Category', 'Taxonomy Singular Name', 'rast'),
            'menu_name' => __('Category', 'rast'),
            'all_items' => __('All Categories', 'rast'),
            'parent_item' => __('Parent Category', 'rast'),
            'parent_item_colon' => __('Parent Category:', 'rast'),
            'new_item_name' => __('New Category Name', 'rast'),
            'add_new_item' => __('Add New Category', 'rast'),
            'edit_item' => __('Edit Category', 'rast'),
            'update_item' => __('Update Category', 'rast'),
            'view_item' => __('View Category', 'rast'),
            'separate_items_with_commas' => __('Separate categories with commas', 'rast'),
            'add_or_remove_items' => __('Add or remove categories', 'rast'),
            'choose_from_most_used' => __('Choose from the most used', 'rast'),
            'popular_items' => __('Popular Categories', 'rast'),
            'search_items' => __('Search Categories', 'rast'),
            'not_found' => __('Not Found', 'rast'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        );
        register_taxonomy('work_category', array('work'), $args);
    }

    add_action('init', 'work_category', 0);
}

function wallpaper_per_page_query( $query ){
		$query->set( 'posts_per_page', 12 );
}

add_action( 'pre_get_posts', 'wallpaper_per_page_query' );
add_action( 'init', 'printed_walls' );

function printed_walls() {

	add_shortcode( 'show_walls', 'printed_walls_category' );
}

function printed_walls_category() {
// Listar las taxonomías 'post_tag' y 'my_tax'
/* $taxonomies = array( 
    'wallpapers-category'
); */

$args_home = array(
	'taxonomy' => 'wallpapers-category',
    'orderby'           => 'name', 
    'order'             => 'ASC',
	 'parent'   		=> 0,
    'hide_empty'        => true,
    'number'            => 9,
    'cache_domain'      => 'core'
);
$args = array(
	'taxonomy' => 'wallpapers-category',
    'orderby'           => 'name', 
    'order'             => 'ASC',
    'hide_empty'        => true,
	 'parent'     		=> 0,
    'number'            => '',
    'cache_domain'      => 'core'
);
if ( is_front_page() ) {
$terms = get_terms($args_home);
}else {
$terms = get_terms($args);
}
?>
<div class="wall_section">
<div class="row">
<?php
$n=0;
foreach($terms as $term){
	$term_id = $term->term_id;
	$term_name = $term->name;
	if ($n % 3 == 0) {
	?>
	</div>
	<div class="row">
	<?php
	}
	?>
	<div class="col-sm-4"> <?php
	?>
	<a href="<?php echo get_term_link($term_id); ?>"> <?php the_term_thumbnail( $term_id, $size = 'post-thumbnail'); 
	?>
	<p><?php echo $term_name;?> </p>
	</a>
	</div>
	<?php
	if ( is_front_page() ) {
		if ($n==8){
			?>
			<div class="col-sm-12 view_more"> <?php
			?>
			<a href="<?php echo site_url().'/printedwalls-categories/'?>"><img src="http://printedwalls.co.uk/wp-content/uploads/2016/09/view-categories.png"></a>
			</div>
			<?php
		}	
	}
	$n++;

}
?>
</div>
</div>
<?php
}
/*
Login Section
*/
function wpse_19692_registration_redirect() {
    return home_url( '/register' );
}

add_filter( 'registration_redirect', 'wpse_19692_registration_redirect' );

add_filter('login_errors','login_error_message');
function login_error_message($error){
    //check if that's the error you are looking for
    $pos = strpos($error, 'incorrect');
    if (is_int($pos)) {
        //its the right error so you can overwrite it
        $error = "The username or password entered are incorrect, please try again.";
    }
    return $error;
}

/* Main redirection of the default login page */
function redirect_login_page() {
	$login_page  = home_url('/login/');
	$page_viewed = basename($_SERVER['REQUEST_URI']);

	if($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		wp_redirect($login_page);
		exit;
	}
}
add_action('init','redirect_login_page');

/* Where to go if a login failed */
function custom_login_failed() {
	$login_page  = home_url('/login/');
	wp_redirect($login_page . '?login=failed');
	exit;
}
add_action('wp_login_failed', 'custom_login_failed');

/* Where to go if any of the fields were empty */
function verify_user_pass($user, $username, $password) {
	$login_page  = home_url('/login/');
	if($username == "" || $password == "") {
		wp_redirect($login_page . "?login=empty");
		exit;
	}
}
add_filter('authenticate', 'verify_user_pass', 1, 3);

function my_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		$redirect_to = home_url();
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url();
		}
	} else {
		$redirect_to = home_url();
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
/* What to do on logout */
function logout_redirect() {
	$login_page  = home_url('/login/');
	wp_redirect($login_page . "?login=false");
	exit;
}
add_action('wp_logout','logout_redirect');
/* add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +
 
function woo_archive_custom_cart_button_text() {
 
        return __( 'MY CART', 'woocommerce' );
 
} */

/*
End Login Section
*/