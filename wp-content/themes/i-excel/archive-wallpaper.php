<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 
$term =	$wp_query->queried_object;
	$term_id = $term->term_id;
	$term_description = $term->description;
	
?>


<!-- News -->
<section class="pt-50">
    <div class="container">
	
		<div class="row cat_head">
		<div class="col-sm-3">
		<?php
		the_term_thumbnail( $term_id, $size = 'post-thumbnail');
		?>
		</div>
		<div class="col-sm-9">
		  <h3><?php echo $term->name; ?></h3>
		  <p><?php echo $term->description; ?></p>
		</div>
		</div>

	  
        <div class="clearfix"></div>
        <div class="col_container">
        <?php if ( have_posts() ) : 
            // Start the Loop.
            while ( have_posts() ) : the_post();
        ?>
		
		<div class="col-sm-3">
            <div class="news-box">
                <div class="img-holder">
                    <?php
                    if ( has_post_thumbnail() ) : 
                    the_post_thumbnail();  
                    endif; 
                    ?>
                    <a href="#" class="overlay">&nbsp;</a>
                </div>
                <div class="news-meta">
                    <span class="news-date"><?php echo get_the_date( 'j F', $post->ID ); ?></span>
                    <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a> </h3>
                </div>
                <p><?php the_excerpt(); ?></p>
            </div>
        </div>
        <?php
            endwhile;
        endif;
        ?>
		</div>
        <div class="clearfix"></div>
        <!-- Pagination -->
    </div>
</section>
<?php get_template_part('footer-top'); ?>

<?php get_footer();