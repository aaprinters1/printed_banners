<?php
/**
 * The template for displaying wallpaper Archive pages
 *
 * @package WordPress
 * @subpackage Rast
 * @since Rast 1.0
 */

get_header(); 
$term =	$wp_query->queried_object;
$term_id = $term->term_id;
$term_description = $term->description;
$term_parent = $term->parent;
?>


<!-- wallpaper -->
<section class="pt-50">
    <div class="wall_section">
	
		<div class="row cat_head">
		<div class="col-sm-4">
		<?php
		the_term_thumbnail( $term_id, $size = 'post-thumbnail');
		?>
		</div>
		<div class="col-sm-8">
		  <h3><?php echo $term->name; ?></h3>
		  <p><?php echo $term->description; ?></p>
		</div>
		</div>

	  
        <div class="clearfix"></div>
<?php
		$taxonomyName = "wallpapers-category";
		$children = get_categories( array( 'child_of' => $term_id, 'taxonomy' => $taxonomyName ) );
		$child = $children[0];
		//This gets top layer terms only.  This is done by setting parent to 0.  
		$parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) ); 
		$terms = get_terms( $taxonomyName, array( 'number' => 100, 'parent' => $term_id, 'orderby' => 'slug', 'hide_empty' => false ) );
?>
<div class="wall_section">
<div class="col_seprator">
</div>
<?php
	$n=0;
    foreach ( $terms as $term ) {
	$term_id = $term->term_id;
	$term_name = $term->name;
	if ($n % 3 == 0) {
	?>
	</div>
	<div class="row">
	<?php
	}
	?>
	<div class="col-sm-4 wallpaper-box"> 
		<div class="img-holder">
		<a href="<?php echo get_term_link($term_id); ?>"> <?php the_term_thumbnail( $term_id, $size = 'post-thumbnail'); 
		?>
		</div>
		</a>
		<a href="<?php echo get_term_link($term_id); ?>"><p><?php echo $term_name;?> </p>
		</a>
	</div> 
<?php
	$n++;
}
?>
</div>
<?php

if($child){
	get_template_part('footer-top');
}
if($term_parent !=0){
?>
	<div class="wall_section">
        <div class="col_seprator">
        <?php 
		if ( have_posts() ) :
		$n=0;
            // Start the Loop.
            while ( have_posts() ) : the_post();
		if ($n % 3 == 0) {
			?>
		</div>
			<div class="row">
				<?php
				}
				?>
			<div class="col-sm-4">
				<div class="wallpaper-box">
					<a href="<?php echo esc_url( get_permalink() ); ?>">
						<div class="img-holder">
							<?php
							if ( has_post_thumbnail() ) : 
							the_post_thumbnail();  
							endif; 
							?>
						</div>
						<div class="wallpaper-meta">
							<p><?php the_title(); ?></p>
						</div>
					</a>
				</div>
			</div>
			<?php
			$n++;
				endwhile;
			?>
			<div class="row">
				<div class="col-sm-12">
				<?php
				iexcel_paging_nav();
				?>
				</div>
			</div>
		</div>
	<?php
        endif;
        ?>
	</div>
	<?php 
	get_template_part('footer-top'); ?>
</div>
	<?php
	}
	elseif(empty($child)){
	?>
		<div class="wall_section">
		<div class="col_seprator">
        <?php if ( have_posts() ) :
		$n=0;
            // Start the Loop.
            while ( have_posts() ) : the_post();
		if ($n % 3 == 0) {
			?>
		</div>
			<div class="row">
				<?php
				}
				?>
		<div class="col-sm-4">
            <div class="wallpaper-box">
				<a href="<?php echo esc_url( get_permalink() ); ?>">
					<div class="img-holder">
						<?php
						if ( has_post_thumbnail() ) : 
						the_post_thumbnail();  
						endif; 
						?>
					</div>
					<div class="wallpaper-meta">
						<p><?php the_title(); ?></p>
					</div>
				</a>
            </div>
        </div>
        <?php
			$n++;
				endwhile;
				?>
			<div class="row">
				<div class="col-sm-12">
				<?php
				iexcel_paging_nav();
				?>
				</div>
			</div>
		</div>
	<?php
        endif;
        ?>
	</div>
	<?php get_template_part('footer-top'); ?>
	<? } ?>
	</div>
        <div class="clearfix"></div>
        <!-- Pagination -->
    </div>
</section>
<?php get_footer();
