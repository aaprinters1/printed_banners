<?php
/**
 * Template Name: Work
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
 
// Work WP_Query arguments
?>
<?php
get_header();
$work_args = array(
    'post_type' => 'work',
    'post_status' => array('publish'),
    'posts_per_page' => -1,
    'ignore_sticky_posts' => false,
    'order' => 'ASC',
    'orderby' => 'menu_order',
);

// The Query
$work_query = new WP_Query($work_args);
if ($work_query->have_posts()){
    $args = array(
		'type' => 'post',
        'child_of' => 0,
        'parent' => '',
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => 1,
        'hierarchical' => 0,
        'exclude' => '',
        'include' => '',
        'number' => '',
        'taxonomy' => 'work_category',
        'pad_counts' => false
    );
    $work_categories = get_categories($args);
	$work_cats_print = '';
    if (isset($work_categories) && !empty($work_categories) && !is_wp_error($work_categories)) {
        foreach ($work_categories as $work_category) {
            $work_cats_print .= sanitize_title($work_category->name) . ' ';
        }
    }	
	?>
	
	<section id="work" class="default-work">
        <h2 id="work-h2">&nbsp;PrintedWalls Work&nbsp;</h2>
		<?php
		while ( have_posts() ) : the_post();
			the_content();
		endwhile;
		?>
		<div class="work_container">
            <div class="clearfix"></div>
            <!--
            Do not remove these lines to make portfolio visible -->

            <div class="portfolioFilter" style="visibility: hidden; height: 1px;">
                <a href="#" data-filter="*" class="current"><?php _e('View All', 'rast'); ?></a>
            </div>
            <!-- Posts -->
            <div id="iso" class="isotope" style="position: relative; overflow: hidden; height: 1140px;">
                <!-- Filters
                 To keep visible filter items on each selection make sure you added data-filter classes to below "visibleFilter" class separating by space -->
                <div class="col-sm-12 visibleFilter isotope-item <?php echo $work_cats_print; ?>">
                    <div class="portfolioFilter">
                        <ul>
                            <li>
                                <a href="#" data-filter="*" class="current"><?php _e('All', 'rast'); ?></a>
                            </li>
                            <?php
                            if (isset($work_categories) && !empty($work_categories) && !is_wp_error($work_categories)) {
                                foreach ($work_categories as $work_category) {
                                    echo '<li>';
                                    echo '<a href="#" data-filter=".' . sanitize_title($work_category->name) . '">' . $work_category->name . '</a>';
                                    echo '</li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <?php
                while ($work_query->have_posts()) :
                    $single_service_classes = array('col-sm-3', ' port', ' isotope-item');
                    $work_query->the_post();

                    //work category list
                    $work_cats = '';
                    $terms = get_the_terms($post->ID, 'work_category');
                    if (isset($terms) && !empty($terms) && !is_wp_error($terms)) {
                        foreach ($terms as $term) {
                            $single_service_classes[] = sanitize_title($term->name) . ' ';
                        }
                    }
                    ?>
                   <div id="post-<?php the_ID(); ?>" <?php post_class($single_service_classes); ?>>
                        <div class="work-img-holder">
                            <?php
                            if (has_post_thumbnail()) :
                                $img = get_the_post_thumbnail( $post_id, 'medium' ); 
								?>
								<a class="wallpaper-link" href="<?php the_post_thumbnail_url('full'); ?>" data-lightbox="example-set" data-title="<?php the_title(); ?>"><?php echo $img; ?> </a> <?php
                            else:
                                echo '<img src="' . get_stylesheet_directory_uri() . '/assets/images/portfolio/1.jpg" alt="">';
                            endif;
                            ?>
                        </div>
                    </div>
                    <?php
                endwhile;
                // Restore original Post Data
                wp_reset_postdata();
                ?>

            </div>
        </div>
        <div class="space50"></div>
    </section>
	<script>
	jQuery(window).load(function () {
        var jQuerycontainer = jQuery('#iso');
        jQuerycontainer.isotope({
                filter: '*',
                animationOptions: {
                    duration: 2000,
                    easing: 'linear',
                    queue: true
                },
				layoutMode: 'masonry',
				masonry: {
                columnWidth: 1
				}
            });
        jQuery('.portfolioFilter a').on('click', function () {
            jQuery('.portfolioFilter .current').removeClass('current');
            jQuery(this).addClass('current');
            var selector = jQuery(this).attr('data-filter');
            jQuerycontainer.isotope({
                filter: selector,
                animationOptions: {
                    duration: 2000,
                    easing: 'linear',
                    queue: true
                },
				layoutMode: 'masonry',
				masonry: {
                columnWidth: 1
				}
            });
            return false;
        });
    });
	</script>
	
<?php
	}
get_template_part('footer-top');
get_footer(); ?>