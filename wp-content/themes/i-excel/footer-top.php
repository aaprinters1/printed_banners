<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package i-excel
 * @since i-excel 1.0
 */
?>
<div class="wpb_text_column wpb_content_element testimonial_section">
	<div class="wpb_wrapper">
		<h2>&nbsp;PrintedWalls&nbsp;Customer Reviews&nbsp;</h2>
		<?php
		echo do_shortcode( '[custom-facebook-feed]' );
		?>
		<h2 class="about">&nbsp;About PrintedWalls&nbsp;</h2>
		<p class="about">I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
		<div class="left_line"></div>
		<div class="right_line"></div>
	</div>
</div>