<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();

while (have_posts()) : the_post();
    ?>
    <div class="row">

        <div class="wall_section col-sm-8">
		 
            <div class="img_container" style="position:relative;">
			<h3 id="wallpaper_title"><?php the_title(); ?></h3>
                <?php
                if (isset($_GET['filename'])) {
                    $attachment = (object) array();
                    $attachment->guid = urldecode($_GET['filename']);
                    $parsed_url = parse_url($attachment->guid);
                    $attachment_path = $_SERVER['DOCUMENT_ROOT'] . $parsed_url['path'];
                } else {
                    $attachment_path = get_attached_file(get_post_thumbnail_id());
                    $attachment = get_post(get_post_thumbnail_id());
                }
                list($width, $height) = getimagesize($attachment->guid);
                $round_hundred = ceil($width / 100) * 100;
                ?>

                <script type="text/javascript">
                    jQuery(function($) {
                        var d = document, ge = 'getElementById';

                        $('#interface').on('cropmove cropend', function(e, s, c) {
                            d[ge]('crop-x').value = c.x;
                            d[ge]('crop-y').value = c.y;
                            d[ge]('crop-w').value = c.w;
                            d[ge]('crop-h').value = c.h;
							var canvas_top = c.y + 42;
							var canvas_left_top = c.x - 32;
							var canvas_left_bottom = c.y + 25;
							var canvas_top_top = canvas_top + 30;
							$("#lable_width").css({"top": canvas_top , "left": c.x});
							$("#lable_height").css({"top": canvas_top_top , "bottom": canvas_left_bottom , "left": canvas_left_top});
                        });

                        $('#target').Jcrop({boxWidth: <?php echo $width; ?>, boxHeight: <?php echo $height; ?>, setSelect: [0, 0, <?php echo $round_hundred; ?>, <?php echo $round_hundred; ?>], aspectRatio: <?php echo get_option('wdyw_default_width'); ?> / <?php echo get_option('wdyw_default_height'); ?>, onSelect: updateCoords});

                        $('#text-inputs').on('change', 'input', function(e) {
                            $('#target').Jcrop('api').animateTo([
                                parseInt(d[ge]('crop-x').value),
                                parseInt(d[ge]('crop-y').value),
                                parseInt(d[ge]('crop-w').value),
                                parseInt(d[ge]('crop-h').value)
                            ]);
                        });

                        $('#design_yout_wall_width, #design_yout_wall_height').focusout(function() {
                            x = document.getElementById('design_yout_wall_width').value;
                            y = document.getElementById('design_yout_wall_height').value;
                            $('#target').Jcrop({boxWidth: <?php echo $width; ?>, boxHeight: <?php echo $height; ?>, setSelect: [0, 0, <?php echo $round_hundred; ?>, <?php echo $round_hundred; ?>], aspectRatio: x / y, onSelect: updateCoords});
                        });

                        $('#design_yout_wall_width, #design_yout_wall_height').keyup(function() {

                            var invalidChars = /[^0-9.]/gi
                            if (invalidChars.test($(this).val())) {
                                $(this).val() = $(this).val().replace(invalidChars, "");
                            }
                            x = document.getElementById('design_yout_wall_width').value;
                            y = document.getElementById('design_yout_wall_height').value;

                            var current_unit = jQuery('#unit').val();
                            switch (current_unit) {
                                case 'foot':
                                    unit_name = "Foot";
                                    foot_width = document.getElementById('design_yout_wall_width').value;
                                    foot_height = document.getElementById('design_yout_wall_height').value;
                                    break;
                                case 'cm':
                                    unit_name = "CM";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 30.48;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 30.48;
                                    break;
                                case 'm':
                                    unit_name = "Meters";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 0.3048000;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 0.3048000;
                                    break;
                                case 'mm':
                                    unit_name = "MM";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 304.8000;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 304.8000;
                                    break;
                                case 'inches':
                                    unit_name = "Inches";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 12;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 12;
                                    break;
                            }

                            total_sq_feet = foot_width * foot_height;
                            total_sq_feet = total_sq_feet.toFixed(2);
                            total_price = sq_feet_price * total_sq_feet;

                            var finishing = jQuery('#finishing').val();
                            if (finishing == "One Piece") {
                                add_finishing_total = one_piece_per * total_price / 100;
                                total_price = add_finishing_total + total_price;
                            }

                            if (total_sq_feet >= 100 && total_sq_feet <= 200) {
                                discount = dis_100_200 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet >= 201 && total_sq_feet <= 300) {
                                discount = dis_201_300 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet >= 301 && total_sq_feet <= 400) {
                                discount = dis_301_400 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet >= 401 && total_sq_feet <= 500) {
                                discount = dis_401_500 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet > 500) {
                                discount = dis_500_plus * total_price / 100;
                                total_price = total_price - discount;
                            }

                            total_price = total_price.toFixed(2);
							document.getElementById('total_product_price').value = total_price;
                            if (x > 0 && y > 0) {
                                $('#target').Jcrop({boxWidth: <?php echo $width; ?>, boxHeight: <?php echo $height; ?>, setSelect: [0, 0, <?php echo $round_hundred; ?>, <?php echo $round_hundred; ?>], aspectRatio: x / y, onSelect: updateCoords});
                                jQuery('#lable_height').html("<p>"+y + "&nbsp;" + unit_name+"</p>");
                                jQuery('#lable_width').html("<p>"+x + "&nbsp;" + unit_name+"</p>");
                                jQuery('#total_price').text(total_price);
                                jQuery('#design_yout_wall_width').focusout();
                            }
                        });

                    });
                </script>
				 <div id="lable_height" style="color: #FFF; font-weight: bold; height: 200px; width: 29px; z-index: 99; position: absolute; left: 0px; bottom: 0px; background: url('http://printedwalls.co.uk/wp-content/uploads/2016/09/height-size.jpg') no-repeat;"><p><?php echo get_option('wdyw_default_height'); ?>&nbsp;Ft</p></div>  
                <div id="interface" class="page-interface" style="position:relative;"><img src="<?php echo $attachment->guid; ?>" id="target">
				</div>
				<div id="lable_width" style="color: #FFF; font-weight: bold; height: 29px; width: 200px; position: absolute; right: 267px; top: 0px; z-index: 99; background: url('http://printedwalls.co.uk/wp-content/uploads/2016/09/width-size.jpg') no-repeat;"><p><?php echo get_option('wdyw_default_width'); ?>&nbsp;Ft</p></div>
               
                <div class="nav-box">
                    <form onsubmit="return false;" id="text-inputs"><span class="input-group"><b>X</b>
                            <input type="text" name="cx" id="crop-x" class="span1"></span><span class="input-group"><b>Y</b>
                            <input type="text" name="cy" id="crop-y" class="span1"></span><span class="input-group"><b>W</b>
                            <input type="text" name="cw" id="crop-w" class="span1"></span><span class="input-group"><b>H</b>
                            <input type="text" name="ch" id="crop-h" class="span1"></span><span class="input-group"><b>Image Path</b>
                            <input type="text" value="<?php echo $attachment_path; ?>" name="ch" id="image-path" class="span1"></span>
                    </form>
                </div>                                                
                <div class="row small-up-4">
                </div>
            </div>
        </div>
        <div class="wall_size col-sm-4">
				<h3>Enter your wall size</h3>
            <div class="wall_unit">
			
                <div class="row">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Units: </label>
                    </div>
                    <div class="small-9 columns">
                        <select id="unit" onchange="javascript:unitConversion(this.value);">                            
                            <option selected="selected" value="foot">Feet</option>
                            <option value="cm">CM</option>
                            <option value="m">Meter</option>
                            <option value="inches">Inches</option>
                            <option value="mm">MM</option>
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_width" class="middle">Width: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_width" value="<?php echo get_option('wdyw_default_width'); ?>">
                    </div>
                </div>




                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_height" class="middle">Height: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_height" value="<?php echo get_option('wdyw_default_height'); ?>">
                    </div>
                </div>
				<div class="row" id="material_option">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Material: </label>
                    </div>
                    <div class="small-9 columns">
                        <select name="material" id="material" onchange="set_material()">
                            <option value="pvc" selected> PVC Vinly 510 GSM</option>
							<option value="polycril_paper">Polycril Paper</option>                          
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Finishing: </label>
                    </div>
                    <div class="small-9 columns">
                        <select name="finishing" id="finishing" onchange="set_finishing();">
                            <option value="tiles">Tiles</option>
                            <option value="one_piece">One Piece</option>                            
                        </select>
                    </div>
                </div>
				<div class="row" id="deliver_option">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Delivery Options: </label>
                    </div>
                    <div class="small-9 columns">
                        <select name="delivery" id="delivery" onchange="set_delivery()">
                            <option value="standard" selected>Standard</option>
							<option value="72hrs">72hrs</option>
                            <option value="48hrs">48hrs</option>                            
                                                        
                        </select>
                    </div>
                </div>
				
                <input type="text" id="total_product_price" value="0" style="display:none;" />
                <input type="text" id="delivery_total_price" value="0" style="display:none;" />
                <input type="text" id="material_total_price" value="0" style="display:none;" />
				<input type="text" id="finishing_total_price" value="0" style="display:none;" />
                <input type="text" id="resetJcrop" onclick="javascript:updateFinishingPrice()" style="display:none;" />
            </div>
            <div class="wall_price">Price : <span ><?php echo get_woocommerce_currency_symbol(); ?><span id="total_price"><?php echo get_option('wdyw_min_price') * get_option('wdyw_default_width') * get_option('wdyw_default_height'); ?></span></span></div>
            <a id="wdyw_add_to_cart" onclick="wdyw_add_to_cart()"><img src="<?php echo plugins_url() . '/wc-design-your-wall/assets/images/add-to-cart-img.png'; ?>" /></a>
        </div>
    </div>

<?php endwhile; ?>
<div class="tab_container">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Information</a></li>
    <li><a data-toggle="tab" href="#menu1">Installation</a></li>
    <li><a data-toggle="tab" href="#menu2">Delivery</a></li>
    <li><a data-toggle="tab" href="#menu3">Copyright</a></li>
    <li><a data-toggle="tab" href="#menu3">Print</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
	  <p><?php 
	  if (is_single( '5' )){
		  echo "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.";
	  }else{
	  the_content();
	  } ?></p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
	<div id="menu4" class="tab-pane fade">
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>
</div>
<?php get_template_part('footer-top'); ?>
<?php get_footer(); ?>