<?php
/**
 * Template Name: vynyl-banner tabel

 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package i-excel
 * @since i-excel 1.0
 */

get_header();

$posts = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => 'vinyl-banners-sizes',
            )
        )
    )
);
?>
<style>
.col-sm-1 {
    width: 10%;
    float: left;
}
a.a_vprice{
	width: 47px;
    display: block;
    float: left;
}
.dropt td {
    color: white!important;
}
.mm_container,.cm_container{
	display:block;
 width: 75%;
}
div#foot_container_all {
    width: 75%;
}
</style>
<div class="mm_container">
<div class="col_seprator">
<?php
$count = 9;
foreach($posts as $post){
	$post_id = $post->ID;
	$post_name = $post->post_title;
	$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'MM');
if($pos==6 || $pos==7){
	if($count%14 == 0 ){
	?>	
	</div>
	<?php 
	if($count == 14 ){
		?>
	<div class="col-sm-1">
	<div>Price in £'s</div>
	<div>600MM</div>
	<div>1000MM</div>
	<div>1200MM</div>
	<div>1500MM</div>
	<div>2000MM</div>
	<div>2250MM</div>
	<div>2500MM</div>
	<div>2750MM</div>
	<div>3000MM</div>
	<div>3500MM</div>
	<div>4250MM</div>
	<div>5000MM</div>
	<div>5500MM</div>
	<div>6000MM</div>
	</div>
	<?php
	}
	?>
	<div class="col-sm-1">
	<?php
	$headsize =substr($size, 2, -9);
	print_r('<div>'.$headsize.'</div>');
	}
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
?>
<div class="dropt" title="">
	<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
	<div>
		<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
		<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
		<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
		<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
		<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
		<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
	</div>
</div>
	<?php
}
$count++;
}
?>
</div>
</div>
<div class="cm_container">
<div class="col_seprator">
<?php
$cm_count = 9;
foreach($posts as $post){
	$post_id = $post->ID;
	$post_name = $post->post_title;
	$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'CM');
if($pos==5 || $pos==6){
	if($cm_count % 14 == 0 ){
	?>
	</div>
	<?php
	if($cm_count == 266){
		?>
	<div class="col-sm-1">
	<div>Price in £'s</div>
	<div>60CM</div>
	<div>100CM</div>
	<div>120CM</div>
	<div>150CM</div>
	<div>200CM</div>
	<div>225CM</div>
	<div>250CM</div>
	<div>275CM</div>
	<div>300CM</div>
	<div>350CM</div>
	<div>425CM</div>
	<div>500CM</div>
	<div>550CM</div>
	<div>600CM</div>
	</div>
	<?php
	}
	?>
	<div class="col-sm-1">
	<?php
	$headsize =substr($size, 2, -8);
	print_r('<div>'.$headsize.'</div>');
	}
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
?>
<div class="dropt" title="">
	<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
	<div>
		<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
		<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
		<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
		<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
		<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
		<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
	</div>
</div>
	<?php
}
$cm_count++;
}
?>
</div>
</div>
<div id ="foot_container_all" class="ft_container">
<div class="col_seprator">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'product_cat' => '2-foot-width-banners',
    'orderby'   => 'meta_value_num',
    'meta_key'  => '_price',
    'order' => 'asc'
    );
$posts1 = new WP_Query( $args );
$ft_count = 8;
$post_arr = $posts1->posts;
foreach($post_arr as $post){
$post_id = $post->ID;
$post_name = $post->post_title;
$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'Foot');
if($pos == 4 || $pos == 5){
	if($ft_count == 14){
		?>
	<div class="col-sm-1">
	<div>Price in £'s</div>
	<div>2 Ft</div>
	<div>3 Ft</div>
	<div>4 Ft</div>
	<div>5 Ft</div>
	<div>6 Ft</div>
	<div>7 Ft</div>
	<div>8 Ft</div>
	<div>9 Ft</div>
	<div>10 Ft</div>
	<div>12 Ft</div>
	<div>14 Ft</div>
	<div>16 Ft</div>
	<div>18 Ft</div>
	<div>20 Ft</div>
	</div>
	<?php
	}
	if($ft_count % 14 == 0 ){
	?>
	<div class="col-sm-1">
	<?php
	print_r('<div> 2 Ft </div>');
	}
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
	?>
	<div class="dropt" title="">
		<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
		<div>
			<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
			<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
			<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
			<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
		</div>
	</div>
<?php	
}
$ft_count++;
}
?>
</div>
</div>
<div class="ft_container">
<div class="col_seprator">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'product_cat' => '3-foot-width-banners',
    'orderby'   => 'meta_value_num',
    'meta_key'  => '_price',
    'order' => 'asc'
    );
$posts1 = new WP_Query( $args );
$ft_count = 14;
$post_arr = $posts1->posts;
foreach($post_arr as $post){
$post_id = $post->ID;
$post_name = $post->post_title;
$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'Foot');
if($pos == 4 || $pos == 5){
	if($ft_count % 14 == 0 ){
	?>
	<div class="col-sm-1">
	<?php
	print_r('<div> 3 Ft </div>');
	}
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
	?>
	<div class="dropt" title="">
		<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
		<div>
			<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
			<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
			<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
			<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
		</div>
	</div>
<?php	
}
$ft_count++;
}
?>
</div>
</div>
<div class="ft_container">
<div class="col_seprator">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'product_cat' => '4-foot-width-banners',
    'orderby'   => 'meta_value_num',
    'meta_key'  => '_price',
    'order' => 'asc'
    );
$posts1 = new WP_Query( $args );
$ft_count = 14;
$post_arr = $posts1->posts;
foreach($post_arr as $post){
$post_id = $post->ID;
$post_name = $post->post_title;
$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'Foot');
if($pos == 4 || $pos == 5){
	if($ft_count % 14 == 0 ){
	?>
	<div class="col-sm-1">
	<?php
	print_r('<div> 4 Ft </div>');
	}
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
	?>
	<div class="dropt" title="">
		<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
		<div>
			<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
			<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
			<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
			<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
		</div>
	</div>
<?php	
}
$ft_count++;
}
?>
</div>
</div>
<div class="ft_container">
<div class="col_seprator">
<?php
$args = array(
    'post_type' => 'product',
    'posts_per_page' => -1,
    'product_cat' => '5-foot-width-banners',
    'orderby'   => 'meta_value_num',
    'meta_key'  => '_price',
    'order' => 'asc'
    );
$posts1 = new WP_Query( $args );
$ft_count = 14;
$post_arr = $posts1->posts;
foreach($post_arr as $post){
$post_id = $post->ID;
$post_name = $post->post_title;
$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'Foot');
if($pos == 4 || $pos == 5){
	if($ft_count % 14 == 0 ){
	?>
	<div class="col-sm-1">
	<?php
	print_r('<div> 5 Ft </div>');
	}
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
	?>
	<div class="dropt" title="">
		<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
		<div>
			<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
			<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
			<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
			<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
		</div>
	</div>
<?php	
}
$ft_count++;
}
?>
</div>
</div>
<div class="ft_container">
<div class="col_seprator">
<?php
$ft_count = 14;
asort($posts);
foreach($posts as $post){
	$post_id = $post->ID;
	$post_name = $post->post_title;
	$post_url = $post->guid;
$sale = get_post_meta( $post_id, '_sale_price'); 
$price = get_post_meta( $post_id, '_regular_price');
$sale_price = $sale[0];
$regular_price = $price[0];
$str = array("Vinyl Banner", "Print Banner");
$size = str_replace($str," ",$post_name);
if($sale_price){
	$price= $sale_price; 
} else {
	$price= $regular_price;
}
$pos = strpos($size,'Foot');
if($pos==4 || $pos==5){
	if($ft_count % 14 == 0 ){
	$headsize =substr($size, 2, -9);
	if($headsize == '10 Foot' || $headsize == '9 Foot' || $headsize == '8 Foot' || $headsize == '7 Foot' || $headsize == '6 Foot'){
	?>
	</div>
	<div class="col-sm-1">
	<?php
	print_r('<div>'.$headsize.'</div>');
	}
	}
	if($headsize == '10 Foot' || $headsize == '9 Foot' || $headsize == '8 Foot' || $headsize == '7 Foot' || $headsize == '6 Foot'){
	print_r('<a class="a_vprice" href="'.$post_url.'"><div class="v_price">£'.$price.'</div></a>');
	?>
	<div class="dropt" title="">
		<img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2014/12/b-icon.png" alt="" width="" align="middle" height=""> 
		<div>
			<p class="ptemhom"><?php print_r('RRP £'.$price);?></p>
			<span style="line-height: 30px;color: #eaf000 !important;"><span class="homestyln">Quantity</span><span class="homestylnow">Price / Banner</span> </span><br>
			<table><tbody><tr><th>1-2 </th><td> <?php print_r('£ '.$price);?></td></tr></tbody></table>
			<table><tbody><tr><th>3-5 </th><td><?php $first_discount=$price*67/100; print_r('£ '.$first_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>6-8 </th><td><?php $second_discount=$price*64/100; print_r('£ '.$second_discount);?> </td></tr></tbody></table>
			<table><tbody><tr><th>9-15 </th><td><?php $third_discount=$price*62/100; print_r('£ '.$third_discount);?> </td></tr></tbody></table>
		</div>
	</div>
<?php	
	}
}
$ft_count++;
}
?>
</div>
<?php
get_footer();
?>