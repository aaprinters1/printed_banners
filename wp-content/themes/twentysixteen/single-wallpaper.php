<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();

while (have_posts()) : the_post();
    ?>

    <div class="row">

        <div class="medium-6 columns">
            <div style="position:relative;">
                <h3><?php the_title(); ?></h3>
                <div id="lable_width" style="color: #FFF; font-weight: bold; height: 29px; width: 200px; position: absolute; right: 267px; top: 55px; z-index: 9999999; background: url(<?php echo plugins_url() . '/wc-design-your-wall/assets/images/width-size.jpg'; ?>) no-repeat;"><?php echo get_option('wdyw_default_width'); ?> Foot</div>
                <div id="lable_height" style="color: #FFF; font-weight: bold; height: 200px; width: 29px; z-index: 9999999; position: absolute; left: 58px; bottom: 155px; background: url(<?php echo plugins_url() . '/wc-design-your-wall/assets/images/height-size.jpg'; ?>) no-repeat;"><?php echo get_option('wdyw_default_height'); ?> Foot</div>   
                <?php
                if (isset($_GET['filename'])) {
                    $attachment = (object) array();
                    $attachment->guid = urldecode($_GET['filename']);
                    $parsed_url = parse_url($attachment->guid);
                    $attachment_path = $_SERVER['DOCUMENT_ROOT'] . $parsed_url['path'];
                } else {
                    $attachment_path = get_attached_file(get_post_thumbnail_id());
                    $attachment = get_post(get_post_thumbnail_id());
                }
                list($width, $height) = getimagesize($attachment->guid);
                $round_hundred = ceil($width / 100) * 100;
                ?>

                <script type="text/javascript">
                    jQuery(function($) {
                        var d = document, ge = 'getElementById';

                        $('#interface').on('cropmove cropend', function(e, s, c) {
                            d[ge]('crop-x').value = c.x;
                            d[ge]('crop-y').value = c.y;
                            d[ge]('crop-w').value = c.w;
                            d[ge]('crop-h').value = c.h;
                        });

                        $('#target').Jcrop({boxWidth: <?php echo $width; ?>, boxHeight: <?php echo $height; ?>, setSelect: [0, 0, <?php echo $round_hundred; ?>, <?php echo $round_hundred; ?>], aspectRatio: <?php echo get_option('wdyw_default_width'); ?> / <?php echo get_option('wdyw_default_height'); ?>, onSelect: updateCoords});

                        $('#text-inputs').on('change', 'input', function(e) {
                            $('#target').Jcrop('api').animateTo([
                                parseInt(d[ge]('crop-x').value),
                                parseInt(d[ge]('crop-y').value),
                                parseInt(d[ge]('crop-w').value),
                                parseInt(d[ge]('crop-h').value)
                            ]);
                        });

                        $('#design_yout_wall_width, #design_yout_wall_height').focusout(function() {
                            x = document.getElementById('design_yout_wall_width').value;
                            y = document.getElementById('design_yout_wall_height').value;
                            $('#target').Jcrop({boxWidth: <?php echo $width; ?>, boxHeight: <?php echo $height; ?>, setSelect: [0, 0, <?php echo $round_hundred; ?>, <?php echo $round_hundred; ?>], aspectRatio: x / y, onSelect: updateCoords});
                        });

                        $('#design_yout_wall_width, #design_yout_wall_height').keyup(function() {

                            var invalidChars = /[^0-9.]/gi
                            if (invalidChars.test($(this).val())) {
                                $(this).val() = $(this).val().replace(invalidChars, "");
                            }
                            x = document.getElementById('design_yout_wall_width').value;
                            y = document.getElementById('design_yout_wall_height').value;

                            var current_unit = jQuery('#unit').val();
                            switch (current_unit) {
                                case 'foot':
                                    unit_name = "Foot";
                                    foot_width = document.getElementById('design_yout_wall_width').value;
                                    foot_height = document.getElementById('design_yout_wall_height').value;
                                    break;
                                case 'cm':
                                    unit_name = "CM";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 30.48;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 30.48;
                                    break;
                                case 'm':
                                    unit_name = "Meters";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 0.3048000;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 0.3048000;
                                    break;
                                case 'mm':
                                    unit_name = "MM";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 304.8000;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 304.8000;
                                    break;
                                case 'inches':
                                    unit_name = "Inches";
                                    foot_width = document.getElementById('design_yout_wall_width').value / 12;
                                    foot_height = document.getElementById('design_yout_wall_height').value / 12;
                                    break;
                            }

                            total_sq_feet = foot_width * foot_height;
                            total_sq_feet = total_sq_feet.toFixed(2);
                            total_price = sq_feet_price * total_sq_feet;

                            var finishing = jQuery('#finishing').val();
                            if (finishing == "One Piece") {
                                add_finishing_total = one_piece_per * total_price / 100;
                                total_price = add_finishing_total + total_price;
                            }

                            if (total_sq_feet >= 100 && total_sq_feet <= 200) {
                                discount = dis_100_200 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet >= 201 && total_sq_feet <= 300) {
                                discount = dis_201_300 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet >= 301 && total_sq_feet <= 400) {
                                discount = dis_301_400 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet >= 401 && total_sq_feet <= 500) {
                                discount = dis_401_500 * total_price / 100;
                                total_price = total_price - discount;
                            } else if (total_sq_feet > 500) {
                                discount = dis_500_plus * total_price / 100;
                                total_price = total_price - discount;
                            }

                            total_price = total_price.toFixed(2);

                            if (x > 0 && y > 0) {
                                $('#target').Jcrop({boxWidth: <?php echo $width; ?>, boxHeight: <?php echo $height; ?>, setSelect: [0, 0, <?php echo $round_hundred; ?>, <?php echo $round_hundred; ?>], aspectRatio: x / y, onSelect: updateCoords});
                                jQuery('#lable_height').text(y + " " + unit_name);
                                jQuery('#lable_width').text(x + " " + unit_name);
                                jQuery('#total_price').text(total_price);
                                jQuery('#design_yout_wall_width').focusout();
                            }
                        });

                    });
                </script>
                <div id="interface" class="page-interface"><img src="<?php echo $attachment->guid; ?>" id="target"></div>

                <div class="nav-box">
                    <form onsubmit="return false;" id="text-inputs"><span class="input-group"><b>X</b>
                            <input type="text" name="cx" id="crop-x" class="span1"></span><span class="input-group"><b>Y</b>
                            <input type="text" name="cy" id="crop-y" class="span1"></span><span class="input-group"><b>W</b>
                            <input type="text" name="cw" id="crop-w" class="span1"></span><span class="input-group"><b>H</b>
                            <input type="text" name="ch" id="crop-h" class="span1"></span><span class="input-group"><b>Image Path</b>
                            <input type="text" value="<?php echo $attachment_path; ?>" name="ch" id="image-path" class="span1"></span>
                    </form>
                </div>                                                
                <div class="row small-up-4">
                </div>
            </div>
        </div>
        <div class="medium-6 large-5 columns">
            <h4 id="wallpaper_title"><?php the_title(); ?></h4>
            <p><?php the_content(); ?></p>
            <div style="background: rgb(230, 230, 230) none repeat scroll 0% 0%; padding: 16px;">




                <div class="row">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Units: </label>
                    </div>
                    <div class="small-9 columns">
                        <select id="unit" onchange="javascript:unitConversion(this.value);">                            
                            <option selected="selected" value="foot">Foot</option>
                            <option value="cm">CM</option>
                            <option value="m">Meter</option>
                            <option value="inches">Inches</option>
                            <option value="mm">MM</option>
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_width" class="middle">Width: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_width" value="<?php echo get_option('wdyw_default_width'); ?>"> <span class="unit_value">Foot</span>
                    </div>
                </div>




                <div class="row">
                    <div class="small-3 columns">
                        <label for="design_yout_wall_height" class="middle">Height: </label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="design_yout_wall_height" value="<?php echo get_option('wdyw_default_height'); ?>"> <span class="unit_value">Foot</span>
                    </div>
                </div>


                <div class="row">
                    <div class="small-3 columns">
                        <label for="middle-label" class="middle">Finishing: </label>
                    </div>
                    <div class="small-9 columns">
                        <select name="finishing" id="finishing" onchange="javascript:updateFinishingPrice()">
                            <option value="Tiles">Tiles</option>
                            <option value="One Piece">One Piece</option>                            
                        </select>
                    </div>
                </div>

                <input type="text" id="resetJcrop" onclick="javascript:updateFinishingPrice()" style="display:none;" />




            </div>
            <div style="width: 100%; text-align: center; font-weight: bold; padding: 12px; font-size: 32px;">Price : <span style="color:#6A2D8A;"><?php echo get_woocommerce_currency_symbol(); ?><span id="total_price"><?php echo get_option('wdyw_min_price') * get_option('wdyw_default_width') * get_option('wdyw_default_height'); ?></span></span></div>
            <a id="wdyw_add_to_cart" onclick="wdyw_add_to_cart()"><img src="<?php echo plugins_url() . '/wc-design-your-wall/assets/images/add-to-cart-img.jpg'; ?>" /></a>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>