<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'efound_printed_walls');

/** MySQL database username */
define('DB_USER', 'efound_wall');

/** MySQL database password */
define('DB_PASSWORD', 'hassan321');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|BS8H@~j4RY,^jmq2uNY7YRs`f[t;I5|k Vbl.H3MU[C3?B<Zu_]4beWgXGno/w~');
define('SECURE_AUTH_KEY',  '/pc/EO(.lrdc6 eA(20%qmP;==lJ~{(B^%?Kl9^A6Xp2>x?:hC7c**!!&V^aRuue');
define('LOGGED_IN_KEY',    '/`xu=wP,W{/Rj=qBwQmCmXw})UupqKY`T{9W@+mJjPh3tx^!>h+$mVU9#F4i>vJu');
define('NONCE_KEY',        'n}xfO)0T;V>^p/zG>AZu<kJ,hYZOU>.3>PzOK- L%=h4APJ-Cja3&-6LL%hEbx|S');
define('AUTH_SALT',        'G=#[QR,bg y:hS4uGw8,6cPn,n5CsqUNo/c59#,u!^^MjYl9jY&:}Z`+=T&l}eZ*');
define('SECURE_AUTH_SALT', ',1?=}69=h;=...t_BlNG216+c*Et,>Noc%N mhuhO,/w+@VLq8=Jdl5Z4m-^(_5t');
define('LOGGED_IN_SALT',   '?y 1)#[a-A|a Jv$}(1:f.l*B<q0X8uJay_Gm7~TE7bX(rsTf8i(6smWIyezprPW');
define('NONCE_SALT',       '185AzCb_ylKfqz^gi1,.-zBF+B>9-u:{TK^u3{y0eUWUOL623CP}#t|~f|!A0:k,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wallp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
